import { FETCH_POSTS } from "./Types";
import $ from "jquery";

export const fetchPosts = (id, token, offset) => dispatch => {
  $.ajax({
    url:
      `https://api.vk.com/method/wall.get?owner_id=-${id}&count=20&offset=${offset}&access_token=${token}&v=5.95`,
    dataType: "jsonp",
    cache: false,
    success: function(res) {
      if(res.response) {
        return dispatch({
          type: FETCH_POSTS,
          payload: res.response.items,
          offset: offset === 0 ? false : true
        });
      } else {
        return
      }
    },
    error: function(xhr, status, err) {
      console.error(this.props.url, status, err.toString());
    }.bind(this)
  });
};
