import { FETCH_POSTS } from "../actions/Types";

const initialState = {
  items: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_POSTS:
      return {
        ...state,
        items:
          state.items.length > 0 && action.offset
            ? state.items.concat(action.payload)
            : action.payload
      };

    default:
      return state;
  }
}
