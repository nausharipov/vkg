import React from "react";
import { connect } from "react-redux";
import { fetchPosts } from "../actions/PostActions";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Form from 'react-bootstrap/Form';
import Pagination from "react-js-pagination";

const token =
  "f35b6ed5f35b6ed5f35b6ed5daf33047c4ff35bf35b6ed5ae5d55a45c45b482498ec6b3";

class Posts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ownerId: "",
      offset: 0,
      activePage: 1,
      pagePosts: []
    };
    this.handlePageChange = this.handlePageChange.bind(this);
    this.getPosts = this.getPosts.bind(this);
    this.paginate = this.paginate.bind(this);
  }

  componentWillMount() {
    this.getPosts()
  }

  handlePageChange(pageNumber) {
    this.setState({
      activePage: pageNumber,
    });
    setTimeout(this.paginate, 500);
  }

  getPosts() {
    this.props.fetchPosts(this.state.ownerId, token, this.state.offset);
    setTimeout(this.paginate, 500);
  }

  paginate() {
    let posts = this.props.posts;
    this.setState({
      pagePosts: posts.slice(
        (this.state.activePage - 1) * 10,
        ((this.state.activePage - 1) * 10) + 10
      )
    })
  }

  render() {
    var postItems;
    if (this.props.posts) {
      postItems = this.state.pagePosts.map(post => (
        <div key={post.id}>
          <Card style={{ width: "80%", margin: 10 }}>
            <Card.Body>
              <Card.Text>{post.text}</Card.Text>
              <Card.Link href="#">{post.likes.count}</Card.Link>
            </Card.Body>
          </Card>
        </div>
      ));
    }
    return (
      <div class='container'>
        <Form.Label>VK Posts</Form.Label>

        <Pagination
          activePage={this.state.activePage}
          itemsCountPerPage={10}
          totalItemsCount={this.props.posts.length}
          pageRangeDisplayed={10}
          onChange={(p) => {
            this.handlePageChange(p)
          }}
        />
        <form
          onSubmit={event => {
            this.getPosts()
            let initOffset = this.state.offset;
            this.setState({
              offset: initOffset + 20
            });
            setTimeout(this.paginate, 200);
            event.preventDefault();
          }}
        >
          <label>
            ID сообщества или пользователя VK:
            <input
              type="text"
              value={this.state.ownerId}
              onChange={event => {
                this.setState({
                  ownerId: event.target.value,
                  offset: 0
                });
              }}
            />
          </label>
          <input
            type="submit"
            value={this.state.offset === 0 ? "Submit" : "Загрузить еще"}
          />
        </form>
        <Container style={{ justifyContent: "center" }}>{postItems}</Container>

      </div>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.items
});

export default connect(
  mapStateToProps,
  { fetchPosts }
)(Posts);
