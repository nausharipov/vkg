import React from "react";
import "./App.css";
import Posts from "./components/Posts";
import { Provider } from "react-redux";
import store from "./store";

class App extends React.Component {

  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <Posts />
        </Provider>
      </div>
    );
  }
}

export default App;
